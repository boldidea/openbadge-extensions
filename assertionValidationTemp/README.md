# Example usage for assertionValidation extension:

```json
"extensions:assertionValidation": {
    "@context": "https://static.boldidea.org/openbadge/extensions/assertionValidation/context.json",
    "type": ["Extension", "extensions:AssertionValidation"],
    "validations": [
        {
            "path": "evidence.url",
            "validationType": "required",
            "message": "Evidence URL is required"
        }
    ]
}
```
