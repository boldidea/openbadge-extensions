# Example usage for narrativeTemplate extension:

```json
{
  "extensions:narrativeTemplate": {
    "@context": "https://static.boldidea.org/openbadge/extensions/narrativeTemplate/context.json",
    "type": [
      "Extension",
      "extensions:NarrativeTemplate"
    ],
    "evidenceTemplate": {
      "fields": [
        {
          "required": true,
          "type": "text",
          "name": "club",
          "label": "Club name"
        },
        {
          "required": true,
          "type": "text",
          "name": "mentor_name",
          "label": "Mentor name"
        },
        {
          "required": false,
          "type": "textarea",
          "name": "mentor_notes",
          "label": "Mentor notes"
        }
      ],
      "template": "Completed the starter activities and guided project from the ideaSpark Web Development Level 1 course, as verified by a trained Bold Idea mentor.\n\n**Mentor:** {{mentor_name}}  \n**Program:** {{program}}\n{% if mentor_notes %}\n**Mentor notes:**  \n{{mentor_notes}}\n{% endif %}\n"
    }
  }
}
```
